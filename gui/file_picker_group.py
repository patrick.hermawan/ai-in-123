import tkinter as tk


class FilePickerGroup:
    def __init__(self, master, button_browse_command):
        group = tk.Frame(master)
        group.pack(expand=True, fill=tk.X)

        self.entry_path = tk.Entry(
            group,
            width=64,
        )
        self.entry_path.pack(side=tk.LEFT, expand=True, fill=tk.X)

        self.button_browse = tk.Button(
            group,
            text="Browse",
            command=button_browse_command,
        )
        self.button_browse.pack(side=tk.LEFT, fill=tk.X)

    def update_path_from_browse_command(self, path):
        self.entry_path.config(text=path)
        self.entry_path.delete(0, tk.END)
        self.entry_path.insert(0, path)