import tkinter as tk
from os import listdir
from tkinter.filedialog import askdirectory

from gui.file_picker_group import FilePickerGroup
from train import train


class FrameTrain:
    def __init__(self, master):
        self.frame = tk.LabelFrame(master, text="Train", padx=10, pady=10)
        self.frame.pack(expand=True, fill=tk.X)

        self.file_picker_group = FilePickerGroup(
            self.frame,
            button_browse_command=self.action_browse,
        )
        self.label_classes = tk.Label(
            self.frame,
            text="",
        )
        self.label_classes.pack()

        self.button_train = tk.Button(
            self.frame,
            text="TRAIN",
            command=self.action_train,
        )
        self.button_train.pack()

        self.label_training_report = tk.Label(
            self.frame,
            text="Training device: CPU (GPU not yet supported)",
        )
        self.label_training_report.pack()

    def action_browse(self):
        data_dir = askdirectory()
        self.file_picker_group.update_path_from_browse_command(data_dir)

        classes = list(listdir(data_dir))
        self.label_classes.config(
            text="The classes are: " + ", ".join(classes))

    def action_train(self):
        self.button_train.config(state=tk.DISABLED)
        self.label_training_report.config(text="TRAINING... PLEASE WAIT")

        data_dir = self.file_picker_group.entry_path.get()
        accuracy = train(data_dir=data_dir)
        accuracy_percentage = str(round(accuracy * 100))
        self.label_training_report.config(
            text="TRAINING FINISHED!\n" +
                 "Accuracy is " + accuracy_percentage + "%")

        self.button_train.config(state=tk.NORMAL)