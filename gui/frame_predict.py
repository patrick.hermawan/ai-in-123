import tkinter as tk
from tkinter.filedialog import askopenfilename

from PIL import Image, ImageTk

from gui.file_picker_group import FilePickerGroup
from predict import predict


class FramePredict:
    def __init__(self, master):
        self.frame = tk.LabelFrame(master, text="Predict", padx=10, pady=10)
        self.frame.pack(expand=True, fill=tk.X)

        self.file_picker_group = FilePickerGroup(
            self.frame,
            button_browse_command=self.action_browse,
        )
        self.button_predict = tk.Button(
            self.frame,
            text="PREDICT",
            command=self.action_predict,
        )
        self.button_predict.pack()

        self.photo_canvas = tk.Label(self.frame)
        self.label_predict_report = tk.Label(
            self.frame,
            text="",
            font=("Helvetica", 20),
        )

    def action_predict(self):
        self.button_predict.config(state=tk.DISABLED)
        self.label_predict_report.config(
            text="PREDICTING... PLEASE WAIT")

        image_path = self.file_picker_group.entry_path.get()
        self.update_image(image_path=image_path)
        answer = predict(image_path=image_path)
        self.label_predict_report.pack()
        self.label_predict_report.config(text=answer)

        self.button_predict.config(state=tk.NORMAL)

    def update_image(self, image_path):
        image = Image.open(image_path)
        photo_image = ImageTk.PhotoImage(image)
        self.photo_canvas.configure(image=photo_image)
        self.photo_canvas.photo = photo_image
        self.photo_canvas.pack(fill="both", expand="yes")

    def action_browse(self):
        image_path = askopenfilename(
            filetypes=[("Pictures", ".jpg .jpeg .png")])
        self.file_picker_group.update_path_from_browse_command(
            image_path)
        self.update_image(image_path)