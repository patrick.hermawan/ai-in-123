import tkinter as tk

from gui.frame_predict import FramePredict
from gui.frame_train import FrameTrain


class MainWindow:
    def __init__(self):
        window = tk.Tk()
        window.title("AI in 1-2-3")
        tk.Label(text="AI in 1-2-3", font=("Helvetica", 20)).pack()

        self.frame_train = FrameTrain(window)
        self.frame_predict = FramePredict(window)

        window.mainloop()
