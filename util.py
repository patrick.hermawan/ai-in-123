import cv2

ROWS = 64
COLS = 64
CHANNELS = 3
n_x = ROWS * COLS * CHANNELS


def read_image(file_path):
    return cv2.resize(
        cv2.imread(file_path, cv2.IMREAD_COLOR),
        (ROWS, COLS),
        interpolation=cv2.INTER_CUBIC
    )
