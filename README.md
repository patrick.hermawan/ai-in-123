# AI in 1-2-3

### AI as simple as 1-2-3.

All you need to do is to organize the data into separate folders (e.g. a folder 
named Cat and a folder named Dog), and the program will handle the rest.

After training, the file `model.pickle` will be generated, which will be used 
later for prediction. That means, you won't need to perform training every time 
you open the program. As long as the `model.pickle` file exists, you can go 
straight to prediction.

The entire process is done locally, no internet connection required.

## Prerequisites

* [Anaconda](https://www.anaconda.com/download/), [Miniconda](https://conda.io/miniconda.html) (recommended), or [Python](https://www.python.org/downloads)
* (recommended) [PyCharm](https://www.jetbrains.com/pycharm/download/) or [Visual Studio Code](https://code.visualstudio.com/download)
* (recommended) [Git](https://git-scm.com/downloads) or [SourceTree](https://www.sourcetreeapp.com/) (SourceTree already includes a Git package)


## Installation

### Downloading the package

1. Download the AI-in-123 package
    * Using Git:
        1. Open Git Bash
        2. Decide which folder you want this project's folder in. I will use `D:\Git` in this example
        3. Type `cd D:/Git`
        4. Type `git clone https://gitlab.com/patrick.hermawan/ai-in-123.git`
        5. The project will be placed in `D:/Git/ai-in-123`
    * Using SourceTree:
        1. Open a new tab in SourceTree
        2. Enter `https://gitlab.com/patrick.hermawan/ai-in-123.git` in the "Source Path / URL" section
        4. Click on "Clone"

### Preparing the environment (without Docker)

1. Download and install [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) (recommended)
    1. If you are using Windows, DO NOT use WSL as it could not display a GUI
2. Open the Anaconda Prompt
3. Move to the directory of the project
3. Create a new Conda environment by typing `conda create --name aiin123 python=3.9`
4. Install all the necessary components by typing `pip install -r requirements.txt`
5. Conda will output the environment path, save it.
    1. In Linux environment, it will be something like: `/home/<USERNAME>/miniconda3/envs/aiin123/`
    1. In Windows without administrator privilege, it will be something like: `C:\Users\%userprofile%\.conda\envs\aiin123` or `C:\ProgramData\Anaconda\envs\aiin123` (with administrator privilege)
6. Activate the environment by typing `conda activate aiin123`
7. (optional) If you installed a package (e.g using `pip install` or `conda install`), all the new dependancies could be exported as an `requirements.txt` using the following commands:
    1. `pip freeze > requirements.txt`

### Setting up PyCharm (optional)

1. Download and install [PyCharm](https://www.jetbrains.com/pycharm/download/)
2. Open PyCharm and click `File > Open`
3. Navigate to the root of the AI-in-123 folder, in this case `D:\Git\aiin123`
4. Go to `File > Settings > Project Interpreter` and tap on the GEAR icon on the top right corner
5. On the left side, click on `Conda Environment`
6. Click on the `Existing environment` option (instead of `New environment`)
7. Navigate to the conda environment folder:
    1. In Linux environment, it will be something like: `/home/<USERNAME>/miniconda3/envs/aiin123/`
    1. In Windows without administrator privilege, it will be something like: `C:\Users\%userprofile%\.conda\envs\aiin123` or `C:\ProgramData\Anaconda\envs\aiin123` (with administrator privilege)
8. Press OK and select the environment

### Setting up VSCode (optional)

1. Download and install [VSCode](https://code.visualstudio.com/)
2. Activate Python plugin in `Welcome page`
3. After you get your conda-environment set, make sure to press `ctrl + shift + p` and type `python:interpreter`. Then choose `aiin123` environment.

### Generating an executable (.exe) file in Windows

1. Install `auto-py-to-exe` by typing `pip install auto-py-to-exe`
2. Run `auto-py-to-exe` by typing `auto-py-to-exe`
3. In the "Script Location" section, navigate to `gui.py`
3. In the `--hidden-import` section, add `sklearn.utils._weight_vector` to prevent errors
4. Choose "One File" and "Window Based (hide the console)"
5. The command should look something like this: `pyinstaller --noconfirm --onefile --windowed --hidden-import "sklearn.utils._weight_vector"  "trainpredict_gui.py"`