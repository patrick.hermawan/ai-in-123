import pickle

import cv2
import numpy as np
from sklearn.linear_model import LogisticRegressionCV

from util import read_image, n_x


def predict(image_path):
    clf: LogisticRegressionCV = pickle.load(open("model.pickle", 'rb'))
    image = read_image(image_path)
    X = np.ndarray((n_x, 1), dtype=np.uint8)
    X[:, 0] = np.squeeze(image.reshape((n_x, 1)))
    X = X.T
    return clf.predict(X)[0]
