import os
import pickle

import numpy as np
from sklearn.linear_model import LogisticRegressionCV

from util import read_image, n_x


def prep_data(path):
    classes = os.listdir(path)
    path_label_tuple = [
        (os.path.join(path, i, j), i)
        for i in classes
        for j in os.listdir(os.path.join(path, i))
    ]
    m = len(path_label_tuple)

    X = np.ndarray((n_x, m), dtype=np.uint8)
    y = list()
    # print("X.shape is {}".format(X.shape))

    for i, (image_file, label) in enumerate(path_label_tuple):
        image = read_image(image_file)
        X[:, i] = np.squeeze(image.reshape((n_x, 1)))
        y.append(label)
        # if i % 5000 == 0:
        #     print("Proceed {} of {}".format(i, m))

    return X, y


def train(data_dir):
    X_train, y_train = prep_data(data_dir)

    clf: LogisticRegressionCV = LogisticRegressionCV()
    X_train = X_train.T  # TODO CHECK IF NEEDED
    clf.fit(X_train, y_train)
    pickle.dump(
        clf,
        open("model.pickle", "w+b"),
        protocol=pickle.HIGHEST_PROTOCOL
    )

    return clf.score(X_train, y_train)
